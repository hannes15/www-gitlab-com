require 'spec_helper'

describe Gitlab::CodeOwners::UserPresenter do
  subject(:presenter) { described_class.new(team) }

  let(:team) { [{ 'gitlab' => 'john', 'name' => 'John Smith', 'picture' => 'john.png' }] }

  describe '#present' do
    subject { presenter.present(username) }

    context 'when username is a real gitlab name' do
      let(:username) { '@john' }

      it { is_expected.to eq(description: 'John Smith', url: 'https://gitlab.com/john', gitlab_name: '@john', image_url: '/images/team/john-crop.jpg') }
    end

    context 'when username is an unknown gitlab name' do
      let(:username) { '@sam' }

      it { is_expected.to eq(description: '@sam', url: 'https://gitlab.com/sam', gitlab_name: '@sam', image_url: nil) }
    end

    context 'when username is an email' do
      let(:username) { 'john@gitlab.com' }

      it { is_expected.to eq(description: 'john@gitlab.com', url: nil, gitlab_name: nil, image_url: nil) }
    end

    context 'when user has a placeholder image' do
      let(:team) { [{ 'gitlab' => 'john', 'name' => 'John Smith', 'picture' => '../gitlab-logo-extra-whitespace.png' }] }
      let(:username) { '@john' }

      it { is_expected.to eq(description: 'John Smith', url: 'https://gitlab.com/john', gitlab_name: '@john', image_url: '/images/team/../gitlab-logo-extra-whitespace.png') }
    end
  end
end
