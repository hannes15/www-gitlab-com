backend:
 name: gitlab
 repo: gitlab-com/www-gitlab-com # Path to your GitLab repository
 auth_type: implicit # Required for implicit grant
 app_id: # Application ID from your GitLab settings

# Allows access to /admin/ on local dev server
# Run npx netlify-cms-proxy-server from the root directory in separate terminal window
# https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository
local_backend: true

#media_folder: source/images/uploads
#public_folder: /images/uploads

publish_mode: editorial_workflow

media_folder: /source/images/uploads
public_folder: /images/uploads

collections:
  - name: topic
    label: Topic
    format: yml
    extension: yml
    folder: /data/topic/
    create: true
    slug: '{{title}}'
    media_folder: /source/images/topics
    public_folder: /images/topics
    fields:
      - {label: SEO Description, name: description, widget: string, required: true}
      - {label: Canonical Path, name: canonical_path, widget: string, required: true}
      - {label: Social share image, name: twitter_image, widget: image, media_folder: /source/images/opengraph, public_folder: /images/opengraph, required: true}
      - {label: Title, name: title, widget: string, required: true}
      - {label: Header Body, name: header_body, widget: markdown, required: true}
      - label: Related Content, links in header to outside sources
        name: related_content
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: URL, name: url, widget: string, required: false}
      - {label: Cover Image, name: cover_image, widget: image, required: false}
      - {label: Body, name: body, widget: markdown, required: true}
      - label: pull_quote
        name: pull_quote
        widget: list
        required: false
        fields:
          - {label: quote, name: quote, widget: string, required: false}
          - {label: source, name: source, widget: string, required: false}
      - {label: Benefits Title, name: benefits_title, widget: string, required: false}
      - {label: Benefits Description, name: benefits_description, widget: string, required: false}
      - label: benefits list
        name: benefits
        widget: list
        media_folder: /source/images/icons
        public_folder: /images/icons
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: Description, name: description, widget: string, required: false}
          - {label: Image, name: image, widget: image, required: false}     
      - label: cta_banner
        name: cta_banner
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: CTA Body, name: body, widget: markdown, required: false}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
      - {label: Resources title, name: resources_title, widget: string, required: false}
      - {label: Resources description, name: resources_intro, widget: markdown, required: false}
      - label: Resources
        name: resources
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: URL, name: url, widget: string, required: false}
          - {label: Type, name: type, widget: "select", options: ["webcast", "Whitepapers and ebooks", "Case studies", "Reports"], required: false}
      - label: Suggested Content, blog articles only
        name: suggested_content
        widget: list
        required: false
        fields:
          - {label: URL, name: url, widget: string}
      - label: Related Video
        name: related_videos
        widget: list
        required: false
        fields:
          - {label: Name, name: name, widget: string, required: false}
          - {label: Title, name: title, widget: string, required: false}
          - {label: Photo Url, name: photourl, widget: string, required: false}
          - {label: Video Link, name: video_link, widget: string, required: false}
  - name: blog_news
    label: News Blog Posts
    preview_path: "blog/{{year}}/{{month}}/{{day}}/{{filename}}/"
    folder: sites/marketing/source/blog/blog-posts/
    extension: .html.md.erb
    format: frontmatter
    create: true
    slug: '{{title}}'
    filter: {field: "categories", value: "news"}
    media_folder: /source/images/blogimages
    public_folder: /images/blogimages
    fields:
      - {label: Title, name: title, widget: string, required: true}
      - {label: Author, name: author, widget: string}
      - {label: Author GitLab, name: author_gitlab, widget: string}
      - {label: Author Twitter, name: author_twitter, widget: string}
      - {label: categories, name: categories, widget: string}
      - {label: Description, name: description, widget: string}
      - {label: Tags, name: tags, widget: string}
      - {label: Image, name: image_title, widget: image}
      - {label: Twitter Text, name: twitter_text, widget: string}
      - {label: Body, name: body, widget: markdown}
