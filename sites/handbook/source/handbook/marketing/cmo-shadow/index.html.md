---
layout: handbook-page-toc
title: CMO Shadow Program
description: "GitLab Marketing Handbook: CMO Shadow"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## Overview
{: #overview .gitlab-purple}

### Introduction
{: #introduction}

At GitLab, being a CMO shadow is not a job title, but a temporary assignment to shadow the CMO.
The shadows will be present at all meetings of the CMO during their rotation.
GitLab is an [all-remote](/company/culture/all-remote/) company, so the shadow will be attending virtual meetings hosted on Zoom.

### Goal
{: #goal}

The goal of the CMO Shadow Program is to give current and future [TBD(?)](/company/team/structure/) in the Marketing Team an overview of all aspects of the [company](/company/).
This should enable marketing leadership to better perform [global optimizations](/handbook/values/#global-optimization).
You'll gain this context through the [meetings you attend](#meetings) and discussions with the CMO regarding any questions that arise during the call, should time be available during the shadow timeframe.
The program also creates opportunities for the CMO to build relationships with team members across the marketing department and to identify challenges and opportunities earlier.

### What it is not
{: #what-it-is-not}

The CMO Shadow Program is not a performance evaluation or the next step to a promotion. Being a CMO shadow is not needed to get a promotion or a raise, and should not be a consideration factor for a promotion or raise, as diverse applicants have different eligibilities.

### Reasons to participate
{: #reasons-to-participate}

To be developed

### What is it like?
{: #what-it-is-like}

To be developed

### What is the feedback from the CMO?
{: #cmo-feedback}

(To be developed) Hear what our CMO has to say about the CMO Shadow Program.

## Participating in the program
{: #participation .gitlab-purple}

### Eligibility
{: #eligibility}

To be developed

### Rotation schedule
{: #schedule}

| Start date | End date | Shadow & Role |
| ---------- | -------- | ------- |
| 2021-01-04 | 2021-01-08 | [Jackie Gragnola](https://gitlab.com/jgragnola) - Manager, Campaigns |
| 2021-01-04 | 2021-01-08 | [Jackie Gragnola](https://gitlab.com/jgragnola) - Manager, Campaigns and [Leslie Blanchard](https://gitlab.com/lblanchard) - Director, Global Field Marketing |
| 2021-01-04 | 2021-01-08 | [Leslie Blanchard](https://gitlab.com/lblanchard) - Director, Global Field Marketing |

## Preparing for the Program
{: #preparation .gitlab-purple}

### Important things to note
{: #things-to-note}

1. This is not a performance evaluation.
1. Plan to observe and ask questions.
1. Don't plan to do any of your usual work. Prepare your team as if you were on vacation.
1. Be ready to add a number of [handbook](/handbook/handbook-usage/) updates during your shadow period.
1. Participating in the shadow program is a privilege where you will be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present, and future honoring this trust placed in them.
1. Give feedback to and receive feedback from the CMO. Participants in the shadow program are encouraged to deliver [candid feedback](/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback). Examples of this are to the CMO and to the world about the company if they make a blog post or video. Shadows maintaining confidentiality during the program is separate from shadows being able to provide candid feedback.

### What to wear
{: #what-to-wear}

You **do not need to dress formally**; business casual clothes are appropriate. For example, Todd wears a button-up with jeans most days. GitLab shirts are acceptable. Review Todd's calendar to check if there are formal occasions - this may require different clothing. If unsure, please ask the Executive Business Administrator (EBA) in the `#cmo-shadow` Slack channel. <!-- Question: Do we need a slack channel? -->

### Pre-Program Tasks
{: #pre-program-tasks}

#### Practice your introduction
{: #practice-introduction}

You will get asked about yourself during the program, and it's important to describe it correctly. So stand in front of a mirror and practice 3 times. The main point is, do _not_ say that your role is to "follow Todd around" or "follow the CMO around". The program is for exploring and learning about all the parts of GitLab, and there's where the emphasis should lie.

#### Review the CMO's calendar
{: #review-calendar}

Review the CMO's calendar to get an idea of what your upcoming weeks will be like.

#### Include CMO Shadow in Zoom Name
{: #zoom-name}

Shadows should update their name in Zoom to include `[Name] | CMO Shadow` for clarity in meetings.

#### Coffee Chat with CEO Shadow Alumni
{: #alumni-coffee-chat}

Feel free to schedule a coffee chat with any of the CMO Shadow Alumni. You can review the list of [CEO Shadow Alumni](/handbook/ceo/shadow/#alumni) below. These chats can be helpful when deciding whether to apply to participate or if you're unable to participate but want to hear about the experience and what alumni have learned while shadowing.

## What to expect during the program
{: #what-to-expect .gitlab-purple}

### Tasks
{: #tasks}
To be added, if relevant.

### Meetings
{: #meetings}
To be added, if relevant.

### Taking Notes
{: #taking-notes}
To be added, if relevant.

#### Follow activity from the CMO
{: #follow-cmo-activity}

Shadows are encouraged to follow the CMO's activity on various platforms to get a complete picture of his everyday activities and where he directs his attention.

* In Slack: Go to the Slack search bar and type "from:@tbarr" and it will populate the results.
* In GitLab: This can be seen on the CMO's [GitLab activity log](https://gitlab.com/users/tbarr/activity).

## Alumni
{: #zoom-name .gitlab-purple}
<!-- Do we want this? -->
CMO Shadow program alumni are welcome to join the `#cmo-shadow-alumni` Slack channel to stay in touch after the program.

| Start date | End date | Name | Title | Takeaways |
| ---------- | -------- | ---- | ----- | --------- |
| YYYY-MM-DD | YYYY-MM-DD | [TBD]() | TBD | [Youtube Title & Link]() |
