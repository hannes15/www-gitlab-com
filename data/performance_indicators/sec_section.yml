- name: Sec - Section TMAU - Reported Sum of Sec Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across all stages in the Sec Section (Secure, Protect) for All users and Paid users.
  target: 
  org: Sec Section
  section: Sec
  public: true
  pi_type: Section TMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumentated to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982146
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10224291
    dashboard: 758607
    embed: v2
- name: Sec, Secure - Section MAU, SMAU - Unique users who have used a Secure scanner 
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: sec
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumentated to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982579
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9984584
    dashboard: 758607
    embed: v2
- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Tracking correlation between in app clicks and upgrades is extremely difficult with existing product analytics system. Will have to work closely with Product Intelligence and Growth teams to successfully track SAST Core usage upgrades
      - Flywheel of growth driven by SAST and Secret Detection to core continues to hold strong. Nice steady increase of ~10%.
      - We have successfully started tracking [SAST Configuration UI usage](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=10038560&udv=1090066) which will inform prioritization of [Configuration UI for Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/4496). 
  monthly_focus:
    goals:
      - Now that SAST & Secret Detection is availible to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [Improved MR experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) is nearing completion & [Improved Configuration Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/241377) will kickoff in 13.8. Both of these have been slowed by complexities with licensing specific features and differences between code structure in EE and CE. 
      - Pivot focus to some enterprise configuration issues related to [monorepos and advanced configurations](https://gitlab.com/groups/gitlab-org/-/epics/4895) now that we have Static Analysis growing organically. 
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9980130
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9967764
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Steady monthly increase over the last three months (according to PM created charts with GitLab team members removed)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
  lessons:
    learned:
      - Customers have stopped using DAST in the past because of the huge number of vulnerabilities produced in the dashboard
      - Feedback from the effort to aggregate vulnerabilities should result in an uptick in DAST usage
      - More profile options need to be introduced for on-demand DAST scans to have broad usage
  monthly_focus:
    goals:
      - Aggregating noisy vulnerabilities to remove objections to using DAST because it overwhelms the Dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/254043)
      - Add more options to Site profile (https://gitlab.com/groups/gitlab-org/-/epics/3771)
      - DAST Site validation (https://gitlab.com/groups/gitlab-org/-/epics/4871)
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9980137
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9980139
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or  number of unique users who have run one or more License Scanning jobs.
  target: 2% higher than current month (SaaS and self-managed combined) for All and Paid
  org: Sec Section
  section: sec
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Based on discussion with Anoop, who will be documenting all MVC shared understandings, this MVC is as expected and will be shared as a trend and not as ACTUAL numbers and there will be a single source of truth to maintain for the known current accepted data limitations and assumptions.
  lessons:
    learned:
      - Nothing specific to Metrics.
  monthly_focus:
    goals:
      - Nothing directly related to these lagging indicators, but continue work on [auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/3188) as part of [Dependency Scanning to complete](https://gitlab.com/groups/gitlab-org/-/epics/1664).
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9967897
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10102606
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - We have a good growth rate and need more users to use fuzz testing.
  implementation: 
    status: Definition
    reasons:
    - We now have reporting on .com for both coverage and API fuzz testing.
    - Self-managed reporting added for API fuzz testing, waiting for data to report as users upgrade to new GitLab version.
    - Self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118). We _can_ see the number of jobs, just not users.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - We learned about changes needed to usage ping. Since users can set their own job names, we can't look just at job names.
      - Coverage-guided fuzz testing self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118).
  monthly_focus:
    goals:
      - API fuzz testing foundational work to use Security Dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/224726)
      - Corpus management for coverage-guided fuzz testing (https://gitlab.com/groups/gitlab-org/-/epics/2915)
      - Crash reproduction binaries for coverage-guided fuzz testing (https://gitlab.com/gitlab-org/gitlab/-/issues/233937)
      - API Fuzzing of OpenAPI v3 specifications (https://gitlab.com/gitlab-org/gitlab/-/issues/228652)
  metric_name: user_coverage_fuzzing_jobs on self-managed, (options like '%gitlab-cov-fuzz%' OR options like '%FUZZAPI_REPORT%') on .com
  sisense_data:
    chart: 9842394
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >2% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Trending in correct direction again but unsure if above previous threshold (see reasons)
  implementation:
    status: Definition
    reasons:
    - Large Snowplow data loss event in September and October obscure true performance comparison to November
    - Combined with uncertain impact of Thanksgiving holiday week makes true m-o-m performance for November increase impossible
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Data integrity problems make it impossible to make accurate performance and impact assessments until at least December
  monthly_focus:
    goals:
      - Continue executing on roadmap based on input from other sensing mechanisms
      - After successfully passing CMS evaluation for Viable, focus on solution validation for [future design vision](https://gitlab.com/groups/gitlab-org/-/epics/4812)
      - MR security widget [UX improvement](https://gitlab.com/groups/gitlab-org/-/epics/4427)
      - Jira [Integration with Vulnerabilities](https://gitlab.com/groups/gitlab-org/-/epics/4677)
      - Special [References for Vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/281035)
      - Dismissal [types / reasons](https://gitlab.com/gitlab-org/gitlab/-/issues/196976)
      - Bulk [updates](https://gitlab.com/groups/gitlab-org/-/epics/4649)
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of users who have run a container scanning job or interacted with the Protect UI in the last 28 days of the month.
  target: Progressively increasing month-over-month, >2%
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Ignoring October (data was lost that month), we are currently seeing a 1% average month over month growth rate across the two months from September to October.  This is below our target of 2%.
    - Usage growth is projected to continue to decline in the short-term.  See lessons learned section for more details.
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
    - Data is not available yet for the Security Orchestration category as it has not yet reached Minimal maturity
  lessons:
    learned:
      - As Container Security has not been resourced for some time, we have a pent-up list of feature requests and on-going maintenance work that is presenting a strong headwind to increasing usage.
      - Usage growth is anticipated to be minimal or negative until we can make a dent in the outstanding usability challenges.  Work is anticipated to be completed by March, with usage numbers recovering across the March-June timeframe.
      - The release of the Security Orchestration category (planned for January) will also bolster the Group's usage numbers once we instrument metrics tracking for the category.
  monthly_focus:
    goals:
      - Project-level DAST Scan Schedule Policies (https://gitlab.com/groups/gitlab-org/-/epics/4598)
      - Allow container security scans to be run against containers in production environments (https://gitlab.com/groups/gitlab-org/-/epics/3410)
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 10039269
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039569
    dashboard: 758607
    embed: v2
- name: Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >25% month-over-month
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - On track.  Usage grew from 16 clusters to 24 from October to November.  This represents a 50% growth rate and is well above our target of 25%.
  implementation:
    status: Complete
    reasons:
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category is relatively new
  lessons:
    learned:
      - We have 24 clusters using CNS, all of which are on gitlab.com.  Customers are using the blocking mode as a significant amount of traffic is being dropped.
      - CNS is processing far more traffic than WAF did.  At the peak of WAF, we were processing ~46 M packets and blocking ~87 K packets per month.  For comparison, CNS processed 1.6 B packets and dropped 13.5 M packets last month.
      - WAF usage has dropped off significantly since we announced the deprecation and planned removal of the feature.
  monthly_focus:
    goals:
      - Alert Dashboard MVC (https://gitlab.com/groups/gitlab-org/-/epics/3438)
  metric_name: clusters_applications_cilium
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2
 
